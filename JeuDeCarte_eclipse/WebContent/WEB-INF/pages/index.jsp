<%--
  Created by IntelliJ IDEA.
  User: nicol
  Date: 10/12/2016
  Time: 15:06
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl-core" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="card.CardVerificator" %>
<%@ page import="card.PackCards" %>

<html>
<head>
    <title>$Title$</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/JeuDeCarte_eclipse/resources/styles.css"/>
</head>
<body>
<h1>Test à jeu de cartes</h1>

<jstl-core:if test="${empty pack and empty resultTest}" >
    <div style="text-align: center">
        <a class="button" href="/JeuDeCarte_eclipse/start" >start</a>
    </div>
</jstl-core:if>

<jstl-core:if test="${not empty pack}">
    <div style="text-align: center">
        <jstl-core:forEach items="${pack}" var="card">
            <div class="card">
                <span>${card.color}</span>
                <p>
                    <span>${card.value}</span>
            </div>
        </jstl-core:forEach>

        <jstl-core:if test="${empty packSorted}">
            <div style="text-align: center">
                <a class="button" href='/JeuDeCarte_eclipse/sort/<%= CardVerificator.toJSON((PackCards) request.getAttribute("pack")) %>' >trier</a>
            </div>
        </jstl-core:if>
    </div>
</jstl-core:if>

<jstl-core:if test="${not empty packSorted}">
    <div style="text-align: center">
        <jstl-core:forEach items="${packSorted}" var="card">
            <div class="card">
                <span>${card.color}</span>
                <p>
                    <span>${card.value}</span>
            </div>
        </jstl-core:forEach>

        <jstl-core:if test="${empty resultTest}">
            <div style="text-align: center">
                <a class="button" href='/JeuDeCarte_eclipse/test/<%= CardVerificator.toJSON((PackCards) request.getAttribute("packSorted")) %>' >tester</a>
            </div>
        </jstl-core:if>
        <jstl-core:if test="${not empty resultTest}">
            <div style="text-align: center">
                <span class="result">${resultTest}</span>
            </div>
            <div style="text-align: center">
                <a class="button" href="/JeuDeCarte_eclipse/start">restart</a>
            </div>
        </jstl-core:if>
    </div>
</jstl-core:if>

</body>
</html>
