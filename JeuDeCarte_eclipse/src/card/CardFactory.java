package card;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by nicol.
 */
public class CardFactory {

    private CardFactory() {

    }

    private static Card newCard(Card.CardColor color, Card.CardValue value) {
        return new CardImpl(color, value);
    }

    private static PackCards newPackCards(String exerciceId, List<Card.CardColor> colorOrder, List<Card.CardValue> valueOrder) {
        return new PackCardsImpl(exerciceId, colorOrder, valueOrder);
    }

    private static PackCards parseJSON(Reader reader) throws IOException, ParseException {

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(reader);

        String exerciceId = (String) jsonObject.get("exerciceId");
        JSONObject jsonData = (JSONObject) jsonObject.get("data");

        List<Object> cards = (JSONArray) jsonData.get("cards");
        List<String> categoryOrder = (JSONArray) jsonData.get("categoryOrder");
        List<String> valeurOrder = (JSONArray) jsonData.get("valueOrder");

        List<Card.CardColor> colorOrder = categoryOrder.stream()
                .map(Card.CardColor::valueOf)
                .collect(Collectors.toList());

        List<Card.CardValue> valueOrder = valeurOrder.stream()
                .map(Card.CardValue::valueOf)
                .collect(Collectors.toList());

        return cards.stream()
                .map(card -> (JSONObject) card)
                .map(card -> {
                    Card.CardColor color = Card.CardColor.valueOf((String) card.get("category"));
                    Card.CardValue value = Card.CardValue.valueOf((String) card.get("value"));
                    return CardFactory.newCard(color, value);
                })
                .collect(Collector.of(
                        () -> CardFactory.newPackCards(exerciceId, colorOrder, valueOrder),
                        PackCards::add,
                        (left, right) -> { left.addAll(right); return left; }));
    }

    public static PackCards parseJSON(String jsonString) {
        try {
            return parseJSON(new StringReader(jsonString));
        } catch (IOException | ParseException ex) {
            throw new RuntimeException("Erreur : Nico, vérifie JSON string");
        }
    }

    public static PackCards parseJSON(File jsonFile) {
        try {
            return parseJSON(new FileReader(jsonFile));
        } catch (IOException | ParseException ex) {
            throw new RuntimeException("Erreur : Nico, vérifie JSON file");
        }
    }

    public static PackCards parseJSON(URL url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            return parseJSON(new BufferedReader(new InputStreamReader(conn.getInputStream())));
        } catch (IOException | ParseException ex) {
            throw new RuntimeException("Erreur : Nico, vérifie JSON lien");
        }
    }

}
