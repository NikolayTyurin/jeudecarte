package card;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nicol.
 */
public class PackCardsImpl extends ArrayList<Card> implements PackCards, Comparator<Card> {

    final private String exerciceId;
    final private List<Card.CardColor> colorOrder;
    final private List<Card.CardValue> valueOrder;

    public PackCardsImpl(String exerciceId, List<Card.CardColor> colorOrder, List<Card.CardValue> valueOrder) {
        this.exerciceId = exerciceId;
        this.colorOrder = colorOrder;
        this.valueOrder = valueOrder;
    }

    @Override
    public String getExerciceId() {
        return exerciceId;
    }

    public List<Card.CardColor> getColorOrder() {
        return colorOrder;
    }

    public List<Card.CardValue> getValueOrder() {
        return valueOrder;
    }

    @Override
    public int compare(Card card1, Card card2) {
        if (getColorOrder().indexOf(card1.getColor()) < getColorOrder().indexOf(card2.getColor()))
            return -1;
        if (getColorOrder().indexOf(card1.getColor()) > getColorOrder().indexOf(card2.getColor()))
            return 1;

        if (getValueOrder().indexOf(card1.getValue()) < getValueOrder().indexOf(card2.getValue()))
            return -1;
        if (getValueOrder().indexOf(card1.getValue()) > getValueOrder().indexOf(card2.getValue()))
            return 1;

        throw new RuntimeException("Piege : 2 cartes égales");
    }

    @Override
    public void sort() {
        super.sort(this);
    }

    @Override
    public String toString() {
        return "PackCards : exerciceId=" + exerciceId + ", colorOrder=" + colorOrder + ", valueOrder=" + valueOrder +
                "\n" + super.toString() + "\n";
    }
}
