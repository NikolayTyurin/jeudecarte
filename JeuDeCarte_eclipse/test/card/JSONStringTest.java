package card;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Created by nicol.
 */
class JSONStringTest {

    @BeforeEach
    void setUp() {
        System.out.println("\nTEST : \n");
    }

    @AfterEach
    void tearDown() {
        System.out.println("FIN de TEST");
    }

    @Test
    void testJSONString1() {
        PackCards pack = CardFactory.parseJSON("{\n" +
                "  \"exerciceId\" : \"584bd407975adeb8520a47b6\",\n" +
                "  \"dateCreation\" : 1481364487946,\n" +
                "  \"candidate\" : {\n" +
                "    \"candidateId\" : \"56715f72975a74a0579d0447\",\n" +
                "    \"firstName\" : \"Anthony\",\n" +
                "    \"lastName\" : \"ALLAIN\"\n" +
                "  },\n" +
                "  \"data\" : {\n" +
                "    \"cards\" : [\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"ACE\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"FIVE\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"JACK\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"THREE\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"JACK\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"KING\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"TWO\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"SEVEN\"}\n" +
                "    ],\n" +
                "    \"categoryOrder\" : [\"DIAMOND\", \"HEART\", \"SPADE\", \"CLUB\"],\n" +
                "    \"valueOrder\" : [\"ACE\", \"TWO\", \"THREE\", \"FOUR\", \"FIVE\", \"SIX\", \"SEVEN\", \"EIGHT\", \"NINE\", \"TEN\", \"JACK\", \"QUEEN\", \"KING\"]\n" +
                "  },\n" +
                "  \"name\" : \"cards\"\n" +
                "}");

        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }

    @Test
    void testJSONString2() {
        PackCards pack = CardFactory.parseJSON("{\n" +
                "  \"exerciceId\" : \"584bd432975adeb8520a47b8\",\n" +
                "  \"dateCreation\" : 1481364530407,\n" +
                "  \"candidate\" : {\n" +
                "    \"candidateId\" : \"56715f72975a74a0579d0447\",\n" +
                "    \"firstName\" : \"Anthony\",\n" +
                "    \"lastName\" : \"ALLAIN\"\n" +
                "  },\n" +
                "  \"data\" : {\n" +
                "    \"cards\" : [\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"ACE\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"SEVEN\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"SIX\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"KING\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"ACE\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"EIGHT\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"TEN\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"TEN\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"SIX\"}\n" +
                "    ],\n" +
                "    \"categoryOrder\" : [\"DIAMOND\", \"HEART\", \"SPADE\", \"CLUB\"],\n" +
                "    \"valueOrder\" : [\"ACE\", \"TWO\", \"THREE\", \"FOUR\", \"FIVE\", \"SIX\", \"SEVEN\", \"EIGHT\", \"NINE\", \"TEN\", \"JACK\", \"QUEEN\", \"KING\"]\n" +
                "  },\n" +
                "  \"name\" : \"cards\"\n" +
                "}");

        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }

    @Test
    void testJSONString3() {
        PackCards pack = CardFactory.parseJSON("{\n" +
                "  \"exerciceId\" : \"584bd466975adeb8520a47ba\",\n" +
                "  \"dateCreation\" : 1481364582085,\n" +
                "  \"candidate\" : {\n" +
                "    \"candidateId\" : \"56715f72975a74a0579d0447\",\n" +
                "    \"firstName\" : \"Anthony\",\n" +
                "    \"lastName\" : \"ALLAIN\"\n" +
                "  },\n" +
                "  \"data\" : {\n" +
                "    \"cards\" : [\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"HEART\", \"value\" : \"EIGHT\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"EIGHT\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"THREE\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"FOUR\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"FIVE\"},\n" +
                "      {\"category\" : \"DIAMOND\", \"value\" : \"QUEEN\"},\n" +
                "      {\"category\" : \"CLUB\", \"value\" : \"TEN\"},\n" +
                "      {\"category\" : \"SPADE\", \"value\" : \"ACE\"}\n" +
                "    ],\n" +
                "    \"categoryOrder\" : [\"DIAMOND\", \"HEART\", \"SPADE\", \"CLUB\"],\n" +
                "    \"valueOrder\" : [\"ACE\", \"TWO\", \"THREE\", \"FOUR\", \"FIVE\", \"SIX\", \"SEVEN\", \"EIGHT\", \"NINE\", \"TEN\", \"JACK\", \"QUEEN\", \"KING\"]\n" +
                "  },\n" +
                "  \"name\" : \"cards\"\n" +
                "}");

        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }
    
}