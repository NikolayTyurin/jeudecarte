package card;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by nicol.
 */
class UrlTest {

    @BeforeEach
    void setUp() {
        System.out.println("\nTEST : \n");
    }

    @AfterEach
    void tearDown() {
        System.out.println("FIN de TEST");
    }

    @Test
    void testUrl() {
        PackCards pack = null;
        try {
            pack = CardFactory.parseJSON(new URL("https://recrutement.local-trust.com/test/cards/56715f72975a74a0579d0447"));
            System.out.println(pack);
            pack.sort();
            System.out.println(pack);
            Assertions.assertTrue(CardVerificator.verifier(pack));
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Erreur : MalformedURLException");
        }
    }

}