package card;

/**
 * Created by nicol.
 */
public interface Card {

    public static enum CardValue {
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
    }

    public static enum CardColor {
        DIAMOND, HEART, SPADE, CLUB
    }

    public CardColor getColor() ;

    public CardValue getValue() ;

}
