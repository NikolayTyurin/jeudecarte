package card;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by nicol.
 */

@Controller
public class CardController {

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/start")
    public String start(Map<String, Object> map) {
        PackCards pack = null;
        try {
            pack = CardFactory.parseJSON(new URL("https://recrutement.local-trust.com/test/cards/56715f72975a74a0579d0447"));
        } catch (MalformedURLException ex) {
            throw new RuntimeException("Erreur : MalformedURLException");
        }
        map.put("pack", pack);

        return "index";
    }

    @RequestMapping(value = "/sort/{pack}")
    public String sort(@PathVariable("pack") String jsonPack,
                       Map<String, Object> map) {

        PackCards pack = CardFactory.parseJSON(jsonPack);

        PackCards packSorted = CardFactory.parseJSON(jsonPack);
        packSorted.sort();

        map.put("pack", pack);
        map.put("packSorted", packSorted);

        return "index";

    }

    @RequestMapping(value = "/test/{packSorted}")
    public String test(@PathVariable("packSorted") String jsonPack,
                       Map<String, Object> map) {

        PackCards pack = CardFactory.parseJSON(jsonPack);

        map.put("packSorted", pack);
        if (CardVerificator.verifier(pack))
            map.put("resultTest", "Tout est bon : code 200");
        else
            map.put("resultTest", "Tout n'est pas bon : code not 200");


        return "index";

    }

}
