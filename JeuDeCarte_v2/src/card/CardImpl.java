package card;

/**
 * Created by nicol.
 */
public class CardImpl implements Card {

    final private CardColor color;
    final private CardValue value;

    public CardImpl(CardColor color, CardValue value) {
        this.color = color;
        this.value = value;
    }

    @Override
    public CardColor getColor() {
        return color;
    }

    @Override
    public CardValue getValue() {
        return value;
    }

    @Override
    public boolean equals(Object c) {
        if (this == c) return true;

        if (c == null) return false;

        if (!(c instanceof Card)) return false;

        Card card = (Card) c;
        return color == card.getColor() && value == card.getValue();
    }

    @Override
    public String toString() {
        return "Card { " + "color=" + color + ", value=" + value + " }";
    }
}
