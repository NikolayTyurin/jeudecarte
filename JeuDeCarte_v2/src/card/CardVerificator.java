package card;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by nicol.
 */
public class CardVerificator {

    private CardVerificator() {

    }

    public static String toJSON(Card card) {
        return "{\"category\" : \"" + card.getColor().toString() + "\"," +
                " \"value\" : \"" + card.getValue().toString() + "\"}";
    }

    public static String toJSON(PackCards pack) {
        StringBuilder str = new StringBuilder();

        str.append("{");

        str.append("\"exerciceId\" : \"").append(pack.getExerciceId()).append("\",");

        str.append("\"data\" : {");

        str.append("\"cards\" : [");
        pack.forEach(card -> str.append(toJSON(card)).append(","));
        str.deleteCharAt(str.length() - 1);
        str.append("],");

        str.append("\"categoryOrder\":[");
        pack.getColorOrder().forEach(color -> str.append("\"").append(color.toString()).append("\","));
        str.deleteCharAt(str.length() - 1);
        str.append("],");

        str.append("\"valueOrder\":[");
        pack.getValueOrder().forEach(value -> str.append("\"").append(value.toString()).append("\","));
        str.deleteCharAt(str.length() - 1);
        str.append("]");

        str.append("}");

        str.append("}");

        return str.toString();
    }

    public static boolean verifier(PackCards pack) {
        try {
            URL url = new URL("https://recrutement.local-trust.com/test/" + pack.getExerciceId());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            String data = ((JSONObject) new JSONParser().parse(toJSON(pack))).get("data").toString();
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            //System.out.println(conn.getResponseCode());
            return (conn.getResponseCode() == 200);
        } catch (IOException | ParseException ex) {
            throw new RuntimeException("Erreur : Vérificator ne marche pas");
        }
    }

}
