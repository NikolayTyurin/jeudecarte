package card;

import java.util.Comparator;
import java.util.List;

/**
 * Created by nicol.
 */
public interface PackCards extends List<Card> {

    public String getExerciceId() ;

    public List<Card.CardColor> getColorOrder() ;

    public List<Card.CardValue> getValueOrder() ;

    public void sort() ;

}
