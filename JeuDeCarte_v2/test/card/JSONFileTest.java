package card;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * Created by nicol.
 */
class JSONFileTest {

    @BeforeEach
    void setUp() {
        System.out.println("\nTEST : \n");
    }

    @AfterEach
    void tearDown() {
        System.out.println("FIN de TEST");
    }

    @Test
    void testJSONFile1() {
        PackCards pack = CardFactory.parseJSON(new File("test/localTest1.json"));
        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }

    @Test
    void testJSONFile2() {
        PackCards pack = CardFactory.parseJSON(new File("test/localTest2.json"));
        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }

    @Test
    void testJSONFile3() {
        PackCards pack = CardFactory.parseJSON(new File("test/localTest3.json"));
        System.out.println(pack);
        pack.sort();
        System.out.println(pack);
        Assertions.assertTrue(CardVerificator.verifier(pack));
    }

}